$(document).ready(function () {
  $(".header-menu__open").click(function () {
        var $this = $(this);

        if($this.hasClass("header-menu__open_opened")) {
            $this.removeClass("header-menu__open_opened");
            $(".header-menu").fadeOut();

            $(".header").removeClass("header_theme_white");
        } else {
            $this.addClass("header-menu__open_opened");
            $(".header-menu").fadeIn();

            $(".header").addClass("header_theme_white");
        }
    });
  $('.reviews_slider').slick({
    dots:true,
    slidesToShow: 3,
    slidesToScrool: 1,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScrool: 1,
          dots: true
        }
      },
      {
        breakpoint: 700,
        settings: {
          slidesToShow: 1,
          slidesToScrool: 1,
          dots: true
        }
      }
    ]
  });
  $("body").on("click", ".open-popup", function (e) {
        e.preventDefault();

        var el = $(this).data("popup");
        var $this = $(this);

        $.fancybox.open($(".popup-" + el), {
            touch: false,
            helpers: {
                thumbs: {
                    width: 50,
                    height: 50
                },
                overlay: {
                    locked: true
                }
            },
            beforeShow: function () {
            },
            afterShow: function () {
            }
        });
    });
  $('.buy-slider').slick({
    arrows:false,
    dots: false,
    asNavFor: '.buy-nav',
    infinite: false
  });

  $('.buy-nav').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: false,
        arrows: false,
        focusOnSelect: true,
        asNavFor: '.buy-slider',
        infinite: false
  });
  $(".buy-colors__item").click(function () {
        var $this = $(this),
            color = $this.data("color");

        $(".buy-colors__item").removeClass("buy-colors__item_active");
        $this.addClass("buy-colors__item_active");


        var colorRu = color == "blue" ? "Синий": "Розовый";
        $(".buy [name=formDesc]").val(colorRu);
  });
  $(".scroll-to").click(function () {
        var to = $(this).data("to") || $(this).attr("href");

        if ($(to).length > 0) {
            $.fancybox.close();

            if (event) event.preventDefault();

            var top = $(to).offset().top + 1;

            top = top - $(".header").innerHeight();

            if (to == "#promo") {
                top = 0;
            }

            if($(document).innerWidth() < 1540) {
                $(".header-menu").fadeOut();
                $(".header-menu__open").removeClass("header-menu__open_opened");

                $(".header").removeClass("header_theme_white");
            }

            $("body, html").animate({scrollTop: top}, 500);
        }
    });
    window.addEventListener("resize", function() {
        if($("html").innerWidth() >= 580) {
            if($(".steps-items").hasClass("slick-initialized")) {
                $(".steps-items").slick("unslick");
            }
        } else {
            if(!$(".steps-items").hasClass("slick-initialized")) {
                $(".steps-items").slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: true,
                    adaptiveHeight: true,
                    arrows: false
                });
            }
        }
    }, false);

    if($("html").innerWidth() < 580) {
        $(".steps-items").slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            adaptiveHeight: true,
            arrows: false
        });
    }
    $(".questions__item").click(function () {
        $this = $(this);

        if ($this.hasClass("questions__item_opened")) {
            $this.removeClass("questions__item_opened");
            $this.find(".questions__item-body").slideUp();
        } else {
            $this.addClass("questions__item_opened");
            $this.find(".questions__item-body").slideDown();
        }
    });
    $("#sendMail").on("click", function(){
      var name = $("#name").val().trim();
      var phone = $("#phone").val().trim();
      var formDesc = $("#horse_color").val().trim();

      if(name == "") {
        $("#errorMess").text("Введите имя");
        return false;
      } else if (phone == ""){
        $("#errorMess").text("Введите телефон");
        return false;
      } else if (formDesc == ""){
        $("#errorMess").text("Выберите цвет");
        return false;
      }

      $("#errorMess").text("");

      $.ajax({
        url: '../ajax/mail.php',
        type: 'POST',
        cache: false,
        data: { 'name': name, 'phone': phone, 'formDesc': formDesc },
        dataType: 'html',
        beforeSend: function () {
          $("#sendMail").prop("disabled", true);
        },
        success: function(data) {
          if(!data)
            alert("Были ошибки, сообщение не отправлено");
          else
            $('#mailForm').trigger("reset");
          $("#sendMail").prop("disabled", false);
          $("#errorMess").text("Сообщение отправлено!");
        }
      });
    });

    $("#sendMail_bottom").on("click", function(){
      var name = $("#name_bottom").val().trim();
      var phone = $("#phone_bottom").val().trim();

      if(name == "") {
        $("#errorMess_bottom").text("Введите имя");
        return false;
      } else if (phone == ""){
        $("#errorMess_bottom").text("Введите телефон");
        return false;
      }

      $("#errorMess_bottom").text("");

      $.ajax({
        url: '../ajax/mail.php',
        type: 'POST',
        cache: false,
        data: { 'name': name, 'phone': phone },
        dataType: 'html',
        beforeSend: function () {
          $("#sendMail_bottom").prop("disabled", true);
        },
        success: function(data) {
          if(!data)
            alert("Были ошибки, сообщение не отправлено");
          else
            $('#mailForm_bottom').trigger("reset");
          $("#sendMail_bottom").prop("disabled", false);
          $("#errorMess_bottom").text("Сообщение отправлено! Мы перезвоним вам в течении 15 минут!");
        }
      });
    });

    $('#phone').mask("+7 (999) 999-99-99");

    var menu_selector = ".header-menu__list";
    function onScroll() {
        var scroll_top = $(document).scrollTop();

        $(menu_selector + " a").each(function () {
            var hash = $(this).attr("href");
            var target = $(hash);
            if (target.length) {
                if (target.position().top - $(".header").innerHeight() <= scroll_top && target.position().top + target.outerHeight() > scroll_top) {
                    $(menu_selector + " .header-menu__list-link").removeClass("header-menu__list-link_active");
                    $(this).addClass("header-menu__list-link_active");
                } else {
                    $(this).removeClass("header-menu__list-link_active");
                }
            }
        });

        var $header = $(".header");

        if (scroll_top > $(".promo").innerHeight() - $(".header").innerHeight()) {
            if (!$header.hasClass("header_fixed")) {
                $header.hide().fadeIn().addClass("header_fixed");
            }
        } else {
            $header.removeClass("header_fixed");
        }
    }

    onScroll();

    $(document).on("scroll", onScroll);

    new WOW().init();

});
