var gulp                      = require('gulp');
var runSequence               = require('gulp4-run-sequence');
var del                       = require('del');

gulp.task('clean', function () {
  return del(['dist/'])
})
gulp.task('build', function (done) {
  runSequence(
    'sass',
    'html',
    'js',
    'fonts',
    'ajax',
    'img',
    'libs'
  );
  done();
});
