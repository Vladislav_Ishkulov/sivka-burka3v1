var gulp                      = require('gulp');
var cnf                       = require('../package.json').config;

gulp.task('ajax', function () {
  return gulp.src(cnf.src.ajax)
    .pipe(gulp.dest(cnf.dist.ajax));
});

gulp.task('ajax:watch', function () {
  gulp.watch(cnf.dist.ajax, gulp.series('ajax'));
});
