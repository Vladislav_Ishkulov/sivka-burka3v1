var gulp                      = require('gulp');
var cnf                       = require('../package.json').config;
var plumber                   = require('gulp-plumber');
var notify                    = require("gulp-notify");
var imagemin                  = require('gulp-imagemin');


gulp.task('img', function (done) {
  gulp.src(cnf.src.img.noCompress)
    .pipe(gulp.dest(cnf.dist.img));
  gulp.src(cnf.src.img.compress)
    .pipe(imagemin([
        imagemin.gifsicle({interlaced: true}),
        imagemin.mozjpeg({quality: 75, progressive: true}),
        imagemin.optipng({optimizationLevel: 5}),
        imagemin.svgo({
            plugins: [
                {removeViewBox: false},
                {cleanupIDs: false}
            ]
        })
    ]))
    .pipe(gulp.dest(cnf.dist.img));
    done();
});

gulp.task('img:watch', function () {
  gulp.watch([cnf.src.img, 'src/img/**/*.*'], gulp.series('img'));
});
