var gulp                      = require('gulp');
var runSequence               = require('gulp4-run-sequence');

gulp.task('default', function(done){
  runSequence(
      ['sass:watch',
      'js:watch',
      'html:watch',
      'fonts:watch',
      'libs:watch',
      'server']
  );
  done();
})
